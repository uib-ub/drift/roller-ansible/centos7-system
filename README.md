Centos7-system
=========

A small role for generic system tasks common for UB
 
* Checks for Centos7/rhel7
* adds a cron job to update and restart the system at a specified time.
* (Optional) add hostname (domain) to server
* (Optional) adds public keys to authorized_keys for box
* (Optional) adds environment variables
* (Optional) adds [IUS](https://ius.io/) default 'false' repo for centos7
* (Optional) adds chronic for system email and sets email in /var/spool/cron jobs
* (Optional) adds volumes (To attach, use os_client_uh-iaas role to create volumes.) 

Requirements
------------
Requires centos 7

Role Variables
--------------
rebootOnChange.hour (default 1) sets the time of task `cron job for yum update and rebooting on kernel changes` 

Dependencies
------------
Depends on system being centos7 if ius_repo is set.

Not tested with rhel7.

Example Playbook
----------------
```
    - hosts: servers
      import_role:
        name: centos7-system
      vars:
        hostname: marcus.uib.no
        ius_repo: false
        ius_remove: true
        epel_repo: true
        centos7_extra_packages: ["yum-utils","imagemagick"]
        ssh_public_keys:
          - "ssh-rsa public-key1"
          - "ssh-rsa public key2"
        environments:
        - { name: "ENV", value: "VALUE" }
        - { name: "ENV2, value: "VALUE" }
        volumes:
        - mount_point: /data
          device: /dev/sdb
        - mount_point: /media
          device: /dev/sdc
```  
License
-------

BSD

Author Information
------------------
